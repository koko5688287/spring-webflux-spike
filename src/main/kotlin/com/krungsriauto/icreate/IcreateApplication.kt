package com.krungsriauto.icreate

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class IcreateApplication

fun main(args: Array<String>) {
	runApplication<IcreateApplication>(*args)
}
